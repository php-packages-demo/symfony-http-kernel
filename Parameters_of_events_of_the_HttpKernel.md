# RequestEvent

||||
|---|---|---|
| HttpKernelInterface | i0    ||
| Request             | i0m   ||
| Type (?int)         | i0    ||
| ?Response           |   iok | to filterResponse |

# Controler resolver
# ControllerEvent

||||
|---|---|---|
| HttpKernelInterface  | i0    ||
| Controler (callable) | i0iok | to argumentResolver and ControllerArgumentsEvent |
| Request              | i0m   ||
| Type (?int)          | i0    ||
| ControllerReflector  |   iok  | to argumentResolver and ControllerArgumentsEvent |
| Attributes           |  io   ||

# Argument resolver
# ControllerArgumentsEvent

||||
|---|---|---|
| HttpKernelInterface | i0    ||
| Controller(Event)   | i0iok | Call controller |
| Arguments           | i0iok | Call controller |
| Request             | i0m   ||
| Type (?int)         | i0    ||
| NamedArguments      |    o  ||
| Attributes          |    o  ||


# Call controller
# ViewEvent extends RequestEvent
  if (!$response instanceof Response)

||||
|---|---|---|
| HttpKernelInterface       | i0    ||
| Request                   | i0m   ||
| Type (int)                | i0    ||
| ControllerResult (mixed)  | i0io  ||
| ?ControllerArgumentsEvent | i0    ||
| ?Response                 |   iok |to filterResponse |


# filterResponse
# ResponseEvent

||||
|---|---|---|
| HttpKernelInterface | i0    ||
| Request             | i0m   ||
| Type (int)          | i0    ||
| Response            | i0iok ||

* i0: constructor argument
* i: input of event
* o: ouput of event
* k: ouput used by HttpKernel
* m: modifiable