# [symfony/http-kernel](https://libraries.io/packagist/symfony%2Fhttp-kernel)

Structured process for converting a Request into a Response
![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/http-kernel)


https://symfony.com/http-kernel

## Official Symfony documentation
* [*Create your own PHP Framework*
  ](https://symfony.com/doc/current/create_framework/index.html)
  * [*The Routing Component*
    ](https://symfony.com/doc/current/create_framework/routing.html)
* [*Routing*](https://symfony.com/doc/current/routing.html)
* [*Built-in Symfony Events*](https://symfony.com/doc/current/reference/events.html)
* [*The HttpKernel Component*
  ](https://symfony.com/doc/current/components/http_kernel.html)
* [*Logging*](https://symfony.com/doc/current/logging.html)
  * [*How to Configure Monolog to Display Console Messages*
    ](https://symfony.com/doc/current/logging/monolog_console.html)
* [*Building your own Framework with the MicroKernelTrait*
  ](https://symfony.com/doc/current/configuration/micro_kernel_trait.html)

## Other documentation
* [*A better ADR pattern for your Symfony controllers*
  ](https://www.strangebuzz.com/en/blog/a-better-adr-pattern-for-your-symfony-controllers)
  2024-11 COil (Strangebuzz)
* [*How to Create a Bundle in Symfony*
  ](https://medium.com/simform-engineering/how-to-create-a-bundle-in-symfony-18bf584835d1)
  2023-10 Bhavin Nakrani (Medium)
* [*Clean controllers in Symfony (III): request handling*
  ](https://dev.to/rubenrubiob/clean-controllers-in-symfony-iii-request-handling-3h79)
  2023-10 Rubén Rubio (DEV)
* [*Clean controllers in Symfony (II): response handling*
  ](https://dev.to/rubenrubiob/clean-controllers-in-symfony-ii-response-handling-2bl1)
  2023-10 Rubén Rubio (DEV)
* [*Clean controllers in Symfony (I): exception handling*
  ](https://dev.to/rubenrubiob/clean-controllers-in-symfony-i-exception-handling-81g)
  2023-09 Rubén Rubio (DEV)
* [*Resolving values for unmapped properties when mapping request data to DTOs in Symfony 6.3+*
  ](https://angelovdejan.me/2023/06/01/resolving-values-for-unmapped-properties-when-mapping-request-data-to-dtos-in-symfony-6-3.html)
  2023-06 Dejan Angelov
* [*Convert dynamically Request content to DTO with Symfony*
  ](https://medium.com/@etearner/how-to-transform-request-content-to-dto-with-symfony-6-2-84c9c8543200)
  2023-05 Etearner
* [*Symfony Request & Response flow*
  ](https://medium.com/@vtla/symfony-request-response-flow-fe5136f9795f)
  2020-05 Alex Vo
* [*How to start build reusable Symfony Bundle*
  ](https://www.contextualcode.com/Blog/How-to-start-build-reusable-Symfony-Bundle)
  April 19, 2019 by Serhey Dolgushev

## Kernel events
* [Parameters of events of the HttpKernel
  ](Parameters_of_events_of_the_HttpKernel.md)

### kernel.request
#### Usage (by popularity)
* [![PHPPackages Rank](http://phppackages.org/p/symfony/http-kernel/badge/rank.svg)](http://phppackages.org/p/symfony/http-kernel)
  HttpKernel\EventListener\RouterListener
* [![PHPPackages Rank](http://phppackages.org/p/symfony/security/badge/rank.svg)](http://phppackages.org/p/symfony/security)
  symfony/security
* [![PHPPackages Rank](http://phppackages.org/p/symfony/security-bundle/badge/rank.svg)](http://phppackages.org/p/symfony/security-bundle)
  symfony/security-bundle

#### Priority
*  8 "security"
* 32 RouterListener

### Note from event dispatcher documentation
"The higher the number, the earlier the listener is called. If two listeners have the same priority, they are executed in the order that they were added to the dispatcher."

## Sub-libraries
### HttpKernel\Log
I am not sure if I would use this code instead of monolog, which may
be more documented or better known by many people?
* See Logging in official Symfony documentation
* https://github.com/symfony/http-kernel/blob/master/Log/Logger.php

## Dependencies deprecations updated
* [*Symfony 5.1 Deprecation RouteCollectionBuilder -> RoutingConfigurator*
  ](https://stackoverflow.com/questions/62197953/symfony-5-1-deprecation-routecollectionbuilder-routingconfigurator)
  (2020-2022) (Stack Overflow)
