<?php

// use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\HttpKernel\EventListener\RouterListener;
use Symfony\Component\HttpKernel\HttpKernel;
// use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator; // for Kernel or MicroKernel
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;


// replace with file to your own project bootstrap
if (file_exists(__DIR__ . '/vendor/autoload.php')):
    include __DIR__ . '/vendor/autoload.php';
else:
    require '/usr/share/php/Symfony/Component/EventDispatcher/autoload.php';
    require '/usr/share/php/Symfony/Component/Routing/autoload.php';
endif;


class HolaController // extends AbstractController
{
    public function __invoke(Request $request): Response
    {
        return new Response(
                        sprintf("Hola %s", $request->get('name'))
                    );
    }
}

$routes = new RouteCollection();
// Why not fluid?
$routes->add('hola', new Route('/hola/{name}', [
    '_controller' => HolaController::class
]));
$routes->add('hello', new Route('/hello/{name}', [
    '_controller' => function (Request $request) {
        return new Response(
            sprintf("Hello %s", $request->get('name'))
        );
    }
]));

$dispatcher = new EventDispatcher();
// Why not fluid?
$dispatcher->addSubscriber(new RouterListener(
    new UrlMatcher(
        $routes
        ,
        new RequestContext()
    )
    ,
    new RequestStack()
));

$kernel = new HttpKernel(
    $dispatcher,
    new ControllerResolver(),
    new RequestStack(),
    new ArgumentResolver()
);

$request = Request::createFromGlobals();

$response = $kernel->handle($request);
$response->send();

$kernel->terminate($request, $response);

